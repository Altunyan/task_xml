@extends('layouts.app')

@section('content')
    <div data-module="currency">
        <div>
            <select id="ISOCodes">
                @foreach($ISOCodes as $code)
                    <option>{{ $code }}</option>
                @endforeach
            </select>
            <input type="text" id="dateTime" value=""/>
            <button type="button" id="filter" class="btn btn-primary">Filter</button>
        </div>
        <div id="chartContainer"></div>
    </div>
@endsection
