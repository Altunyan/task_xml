@extends('layouts.app')

@section('content')
    <div data-module="search">
        <div class="form-group col-md-3">
            <label for="city">Address :</label>
            <input type="text" class="form-control col-md-9" id="city">
            <button class="btn btn-success col-md-3" id="search_btn">Search</button>

            <div id="search_result"></div>
        </div>

        <div id="result_table"></div>
    </div>
@endsection