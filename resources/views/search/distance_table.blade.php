<table class="table table-bordered">
    <thead>
    <tr>
        <th>Distance < 5Km</th>
        <th>Distance From 5 Km to 30Km</th>
        <th>Distance more than 30 Km</th>
    </tr>
    </thead>
    <tbody>
    @for($i = 0; $i <= $largest_array_length; $i++)
        <tr>
            @foreach($cities as $key => $city)
                <td>{{ isset($cities[$key][$i]) ? $cities[$key][$i]->street_name . ' ' . $cities[$key][$i]->address . '(' . $cities[$key][$i]->distance . ')' : '' }}</td>
            @endforeach
        </tr>
    @endfor

    </tbody>
</table>