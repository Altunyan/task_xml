@if(count($cities))
    @foreach($cities as $city)
        <p class="address" data-id="{{ $city->id }}">{{ $city->adm }}  {{$city->street_name }}   {{ $city->address }}</p>
    @endforeach
@else
    <p>Результатов нет</p>
@endif