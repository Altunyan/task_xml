const xhrPromise = require('xhr-promise');
import Chart from 'chart.js';
import daterangepicker from 'daterangepicker';
import select2 from 'select2';

require('bootstrap-select');

class Currency {
    constructor() {
        this.token = document.getElementById('meta_csrf').getAttribute('content');
        this.filter = document.getElementById('filter');

        $('#ISOCodes').select2();
        $('#dateTime').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD',
            }
        });

        this.filter.addEventListener('click', (e) => {
            let isoCodes = document.getElementById('ISOCodes').value,
                dateFilter = document.getElementById('dateTime').value,
                dateFilterArr = dateFilter.split(" - ");

            console.log(e.target);

            this.getCurrency(isoCodes, dateFilterArr);
        });

    }

    getCurrency(isoCodes, dateFilter) {
        new xhrPromise().send({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': this.token,
                'Content-Type': 'application/json'
            },
            url: './get_currency',
            data: JSON.stringify({
                isoCodes: isoCodes,
                dateFilter: dateFilter
            }),
        })
            .then((res) => {
                if (res.responseText.data.length) {
                    let labels = [], data = [], iso = "";
                    iso = res.responseText.data[0].ISO
                    res.responseText.data.forEach(function (element) {
                        labels.push(element.RateDate.split("T")[0])
                        data.push(element.Rate)
                    })
                    this.addChart(labels, data, iso)
                } else {
                    alert('Date range is empty')
                }
            })
    }

    addChart(labels, data, iso) {
        document.getElementById("chartContainer").innerHTML = '<canvas id="chart"></canvas>';
        let ctx = document.getElementById("chart").getContext("2d"),
            myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: iso,
                    data: data,
                    backgroundColor: '#007bffb8',
                    borderColor: '#007bff',
                    borderWidth: 1
                }]
            },
        });
    }

    static _init() {
        let container = document.querySelector('[data-module="currency"]');

        if (container) {
            return new Currency();
        }
    }
}

export default Currency._init;
