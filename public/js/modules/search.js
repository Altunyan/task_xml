const xhrPromise = require('xhr-promise');

class Search {
    constructor() {
        this.token = document.getElementById('meta_csrf').getAttribute('content');
        this.city = document.getElementById('city');
        this.search_btn = document.getElementById('search_btn');
        this.search_result = document.getElementById('search_result');
        this.result_table = document.getElementById('result_table');

        this.search_btn.addEventListener('click', (e) => {
            let name = this.city.value;

            this.getCities(name);
        });

        document.addEventListener('click', (e) => {
            if(e.target.classList.contains('address')) {
                let id = e.target.dataset.id;

                this.getDistanceBetweenCities(id);
            }
        })
    }

    getCities(name) {
        new xhrPromise().send({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': this.token,
                'Content-Type': 'application/json'
            },
            url: './get_cities',
            data: JSON.stringify({name: name}),
        })
            .then((res) => {
                const view = res.responseText.view;

                this.search_result.innerHTML = view;
            })
    }

    getDistanceBetweenCities(id) {
        new xhrPromise().send({
            method: 'POST',
            headers: {
                'X-CSRF-TOKEN': this.token,
                'Content-Type': 'application/json'
            },
            url: './get_distance_between_cities',
            data: JSON.stringify({id: id}),
        })
            .then((res) => {
                const view = res.responseText.view;

                this.result_table.innerHTML = view;
            })
    }

    static _init() {
        let container = document.querySelector('[data-module="search"]');

        if (container) {
            return new Search();
        }
    }
}

export default Search._init;



