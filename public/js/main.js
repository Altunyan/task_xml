//importing js from modules
import initSearch from '../js/modules/search';
import initCurrency from '../js/modules/currency';

//
class Main {
    constructor() {
        initSearch();
        initCurrency();
    }
}

new Main();
