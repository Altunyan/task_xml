<?php

namespace App\Models\MySQL;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = [
        'index',
        'address',
        'street',
        'street_name',
        'street_type',
        'adm',
        'adm1',
        'adm2',
        'cord_x',
        'cord_y',
    ];
}
