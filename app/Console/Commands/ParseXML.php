<?php

namespace App\Console\Commands;

use App\Models\MySQL\City;
use Illuminate\Console\Command;

class ParseXML extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse {filename?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $fields = [
        'id',
        'address',
        'street',
        'street_name',
        'street_type',
        'adm',
        'adm1',
        'adm2',
        'cord_x',
        'cord_y',
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $filename = $this->argument('filename') ? $this->argument('filename') : 'addresses';

        $path = 'xml/' . $filename . '.xml';
        $handle = fopen(base_path('/public/' . $path), "r");
        $contents = fread($handle, filesize(base_path('/public/' . $path)));
        $getData = true;

        $tagname = 'addresses';

        while($getData) {
            $open_tag_position = strpos($contents, "<$tagname>");
            $substr_length = strpos($contents, "</$tagname>") - $open_tag_position + strlen($tagname) + 3;
            $main_tag = substr($contents, $open_tag_position, $substr_length);
            $city = [];

            if($open_tag_position) {
                foreach ($this->fields as $field) {
                    $child_tagname = $tagname . '_' . $field;
                    $child_open_tag_position = strpos($main_tag, "<$child_tagname>");
                    $child_close_tag_position = strpos($main_tag, "</$child_tagname>");

                    $start = $child_open_tag_position + strlen($child_tagname) + 2;
                    $length = $child_close_tag_position - $start;

                    $val = substr($main_tag, $start, $length);

                    $field === 'id' ? $field = 'index' : null;

                    $city[$field] = $val;
                }
                City::create($city);

                $contents = str_replace($main_tag, "", $contents);
            } else {
                $getData = false;
            }
        }

        fclose($handle);
    }
}
