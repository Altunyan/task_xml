<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 15.06.2019
 * Time: 13:06
 */

namespace App\Providers;

use App\Services\CurrencyService;
use Illuminate\Support\ServiceProvider;

class CurrencyServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind('CurrencyService', function(){
            return new CurrencyService();
        });
    }
}