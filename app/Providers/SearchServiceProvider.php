<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 15.06.2019
 * Time: 3:22
 */

namespace App\Providers;

use App\Services\SearchService;
use Illuminate\Support\ServiceProvider;

class SearchServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //
    }

    public function register()
    {
        $this->app->bind('SearchService', function(){
            return new SearchService();
        });
    }
}