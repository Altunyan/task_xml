<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 15.06.2019
 * Time: 13:04
 */

namespace App\Facedes;


use Illuminate\Support\Facades\Facade;

class Currency extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'CurrencyService';
    }
}