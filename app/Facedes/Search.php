<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 15.06.2019
 * Time: 3:09
 */

namespace App\Facedes;


use Illuminate\Support\Facades\Facade;

class Search extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'SearchService';
    }
}