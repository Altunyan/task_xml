<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 13.06.2019
 * Time: 9:16
 */

namespace App\Http\Controllers;

use App\Facedes\Currency;
use App\Facedes\Search;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        return view('search.index');
    }

    public function getCities(Request $request)
    {
        $name = $request->name;

        Search::setCityName($name);

        $view = Search::getCities();

        return response()->json([
            'view' => $view,
        ]);
    }

    public function getDistanceBetweenCities(Request $request)
    {
        $id = $request->id;

        Search::setCityId($id);

        $view = Search::getDistanceBetweenCities();

        return response()->json([
            'view' => $view,
        ]);

    }

    public function getISOCodes()
    {
        $ISOCodes = Currency::getISOCodes();

        return view('cba.index')->with(['ISOCodes'=> $ISOCodes->ISOCodesResult->string]);
    }
    public function getExchange(Request $request)
    {
        $currencies = Currency::getExchangeRateByDateRange($request->dateFilter[0], $request->dateFilter[1], $request->isoCodes);

        return response()->json([
            'data' => $currencies,
        ]);

    }
}
