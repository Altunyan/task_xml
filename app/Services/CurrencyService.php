<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 15.06.2019
 * Time: 13:07
 */

namespace App\Services;


class CurrencyService
{
    /**
     * @var \SoapClient
     */
    private $client;

    private $fields = [
        'Rate',
        'Amount',
        'ISO',
        'Diff',
        'RateDate',
    ];

    /**
     * CurrencyService constructor.
     */
    public function __construct()
    {
        $this->client = new \SoapClient('http://api.cba.am/exchangerates.asmx?WSDL');
    }

    /**
     * get exchange rates latest info
     *
     * @return mixed
     */
    public function getExchangeRatesLatest()
    {
        return $currencies = $this->client->ExchangeRatesLatest();
    }

    /**
     * get exchange rates info by date and currency
     *
     * @param array $data
     * @return mixed
     */
    public function getExchangeRatesByDateAndCurrency(array $data)
    {
        return $this->client->ExchangeRatesByDateByISO($data);
    }

    /**
     * get exchange rates by date range and iso codes
     *
     * @param $dateFrom
     * @param $dateTo
     * @param $ISOCodes
     * @return mixed
     */
     public function getExchangeRateByDateRange($dateFrom, $dateTo, $ISOCodes)
    {
        $result = $this->client->ExchangeRatesByDateRangeByISO([
            'DateFrom' => $dateFrom,
            'DateTo' => $dateTo,
            'ISOCodes' => $ISOCodes  //USD, RUB
        ])->ExchangeRatesByDateRangeByISOResult->any;

        $tagname = 'ExchangeRatesByRange';
        $getData = true;
        $exchanges = [];

        while ($getData) {
            $open_tag_position = strpos($result, "<$tagname");
            $substr_length = strpos($result, "</$tagname") - $open_tag_position + strlen($tagname) + 3;
            $main_tag = substr($result, $open_tag_position, $substr_length);

            if ($open_tag_position) {
                $exchange = [];
                foreach ($this->fields as $field) {
                    $child_open_tag_position = strpos($main_tag, "<$field>");
                    $child_close_tag_position = strpos($main_tag, "</$field>");

                    $start = $child_open_tag_position + strlen($field) + 2;
                    $length = $child_close_tag_position - $start;

                    $val = substr($main_tag, $start, $length);

                    $exchange[$field] = $val;
                }

                $exchanges[] = $exchange;
                $result = str_replace($main_tag, "", $result);
            } else {
                $getData = false;
            }
        }

        return $exchanges;
    }

    /**
     * get iso codes
     *
     * @return mixed
     */
    public function getISOCodes()
    {
        return $this->client->ISOCodes();
    }
}
