<?php
/**
 * Created by PhpStorm.
 * User: Hayk PC
 * Date: 15.06.2019
 * Time: 3:11
 */

namespace App\Services;


use App\Models\MySQL\City;
use Illuminate\Support\Facades\View;

class SearchService
{
    private $city_name;

    private $city_id;

    /**
     * set city name
     *
     * @param $name
     */
    public function setCityName($name)
    {
        $this->city_name = $name;
    }

    /**
     * set selected city id
     *
     * @param $id
     */
    public function setCityId($id)
    {
        $this->city_id = $id;
    }

    /**
     * get cities
     *
     * @return string
     */
    public function getCities()
    {
        $cities = City::where('street_name', 'like', $this->city_name . '%')->get();

        if (!count($cities)) {
            $arr = explode(' ', $this->city_name);

            $cities = City::where(function ($query) use ($arr) {
                $address = $arr[count($arr) - 1];
                array_pop($arr);
                $street = implode(' ', $arr);

                $query->where('street_name', 'like', $street . '%')
                    ->where('address', 'like', $address . '%');
            })
                ->orWhere(function ($query) use ($arr) {
                    $address = $arr[0];
                    array_shift($arr);
                    $street = implode(' ', $arr);

                    $query->where('street_name', 'like', $street . '%')
                        ->where('address', 'like', $address . '%');
                })
                ->get();
        }

        $view = View::make('search.search_result')->with('cities', $cities)->render();

        return $view;
    }

    /**
     * get distance between cities
     *
     * @return string
     */
    public function getDistanceBetweenCities()
    {
        $selected_city = City::find($this->city_id);
        $x = $selected_city->cord_x;
        $y = $selected_city->cord_y;

        $cities = City::all();

        $cities_by_distance = [];

        foreach ($cities as $city) {
            if($city->id != $this->city_id) {
                $distance = number_format($this->distanceInKmBetweenCoordinates($x, $y, $city->cord_x, $city->cord_y), 2, ',', ' ');
                $city->distance = $distance;

                switch ($distance) {
                    case $distance < 5:
                        $key = 0;
                        break;
                    case $distance > 30:
                        $key = 2;
                        break;
                    default:
                        $key = 1;
                }

                $cities_by_distance[$key][] = $city;
            }
        }

        $count = array_map('count', $cities_by_distance);

        ksort($cities_by_distance);

        $view = View::make('search.distance_table')->with([
            'cities' => $cities_by_distance,
            'largest_array_length' => max($count),
        ])->render();

        return $view;
    }

    /**
     * @param $degrees
     * @return float|int
     */
    public function degreesToRadians($degrees) {
        return $degrees * pi() / 180;
    }

    /**
     * get distance by coordinates
     *
     * @param $lat1
     * @param $lon1
     * @param $lat2
     * @param $lon2
     * @return float|int
     */
    public function distanceInKmBetweenCoordinates($lat1, $lon1, $lat2, $lon2) {
        $earthRadiusKm = 6371;

        $dLat = $this->degreesToRadians($lat2-$lat1);
        $dLon = $this->degreesToRadians($lon2-$lon1);

        $lat1 = $this->degreesToRadians($lat1);
        $lat2 = $this->degreesToRadians($lat2);

        $a = sin($dLat/2) * sin($dLat/2) +
            sin($dLon/2) * sin($dLon/2) * cos($lat1) * cos($lat2);

        $c = 2 * atan2(sqrt($a), sqrt(1-$a));
        return $earthRadiusKm * $c;
    }

}