<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'HomeController@index')->name('index');

Route::post('/get_cities', 'HomeController@getCities')->name('get_cities');
Route::post('/get_distance_between_cities', 'HomeController@getDistanceBetweenCities')->name('get_distance_between_cities');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/get_exchange', 'HomeController@getISOCodes')->name('get_exchange');
Route::post('/get_currency', 'HomeController@getExchange')->name('get_currency');
